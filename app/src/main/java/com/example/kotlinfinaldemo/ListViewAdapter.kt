package com.example.kotlinfinaldemo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.sht_item_list.view.*

class ListViewAdapter(var context: Context, var shtList: MutableList<Map<String, String>>) : BaseAdapter() {
    override fun getCount(): Int {
        return shtList.size
    }

    override fun getItem(position: Int): Map<String, String> {
        return shtList.get(position)
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(R.layout.sht_item_list, null)

        var sht = getItem(position)
        view.tvFrmNo.text = sht.get("frm_no")
        view.tvShtNam.text = sht.get("sht_nam")
        view.tvShtTag.text = sht.get("sht_tag")
        view.tvInsertUser.text = sht.get("insert_user")

        // 回傳 View
        return view
    }
}