package com.example.kotlinfinaldemo

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_add_sheet.*

class AddSheetActivity : AppCompatActivity() {
    var context = this
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_sheet)

        var sqlite = SQLiteDB(context)

        // 取消按紐
        btnCancel.setOnClickListener {
            finish()
        }

        // 送出按紐
        btnSubmit.setOnClickListener {
            var frmNo = etShtFrmNo.text.toString()
            var nam = etShtNam.text.toString()
            var tag = etShtTag.text.toString()
            var user = etInsertUser.text.toString()

            if ("".equals(frmNo) || "".equals(nam) || "".equals(tag) || "".equals(user)) {
                Toast.makeText(context, "欄位不能為空。", Toast.LENGTH_SHORT).show()
            } else {
                var chkFrmNo = sqlite.query("sheet", "where frm_no = '" + frmNo + "'", "frm_no")

                // 檢查是否已註冊過
                if (frmNo.equals(chkFrmNo)) {
                    Toast.makeText(context, "表單: " + frmNo + " 已經新增過囉！！", Toast.LENGTH_SHORT).show()
                } else {
                    sqlite.insert(
                        "sheet", " frm_no, sht_nam, sht_tag, insert_user", "'" + frmNo + "', " + "'" + nam + "', " + "'" + tag + "', " + "'" + user + "'")
                    finish()
                    Toast.makeText(context, "表單新增成功。", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}