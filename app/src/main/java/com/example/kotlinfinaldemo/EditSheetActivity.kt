package com.example.kotlinfinaldemo

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_edit_sheet.*

class EditSheetActivity : AppCompatActivity() {
    val context = this
    lateinit var sqlite: SQLiteDB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_sheet)


        sqlite = SQLiteDB(context)

        initShtData()

        // 取消按紐
        btnCancel.setOnClickListener {
            finish()
        }

        // 修改按紐
        btnEdit.setOnClickListener {
            var frmNo = tvFrmNo.text.toString()
            var nam = etShtNam.text.toString()
            var tag = etShtTag.text.toString()
            var user = etInsertUser.text.toString()


            if ("".equals(nam) || "".equals(tag) || "".equals(user)) {
                Toast.makeText(context, "欄位不能為空。", Toast.LENGTH_SHORT).show()
            } else {
                sqlite.update("sheet", " set sht_nam = '$nam', sht_tag = '$tag', insert_user = '$user'", " where frm_no = '$frmNo'")
                finish()
                Toast.makeText(context, "表單更新成功。", Toast.LENGTH_SHORT).show()
            }
        }
    }

    // 載入表單資料
    private fun initShtData() {
        var frmNo = intent.getStringExtra("frm_no")
        tvFrmNo.setText(sqlite.query("sheet", "where frm_no = '" + frmNo + "'", "frm_no"))
        etShtNam.setText(sqlite.query("sheet", "where frm_no = '" + frmNo + "'", "sht_nam"))
        etShtTag.setText(sqlite.query("sheet", "where frm_no = '" + frmNo + "'", "sht_tag"))
        etInsertUser.setText(sqlite.query("sheet", "where frm_no = '" + frmNo + "'", "insert_user"))
    }
}