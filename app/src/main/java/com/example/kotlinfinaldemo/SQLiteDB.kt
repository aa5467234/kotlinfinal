package com.example.kotlinfinaldemo

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.util.Log

class SQLiteDB(context: Context) {
    var db: SQLiteDatabase
    val TAG = "SQLiteDB"

    init {
        var myDBHelper = MyDBHelper(context)
        db = myDBHelper.writableDatabase
        myDBHelper.onCreate(db)
    }

    fun insert(table: String, colomn: String, values: String) {
        var SQL = " insert into $table " + " ( $colomn ) " + " values ( $values) "
        try {
            db.execSQL(SQL)
            Log.i(TAG, "SQL: " + SQL)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun query(table: String, where: String): Cursor? {
        var cursor: Cursor? = null
        var SQL = " select * " + " from $table " + " $where "
        try {
            cursor = db.rawQuery(SQL, null)
            Log.i(TAG, "SQL: " + SQL)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return cursor
    }

    fun query(table: String, where: String, column: String): String {
        var respond = ""
        var SQL = " select * " + " from $table " + " $where "
        try {
            Log.i(TAG, "SQL: " + SQL)

            var cursor = db.rawQuery(SQL, null)
            if (cursor != null) {
                if (cursor.count > 0) {
                    cursor.moveToFirst()
                    respond = cursor.getString(cursor.getColumnIndex(column))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return respond
    }

    fun update(table: String, colomn: String, where: String) {
        var SQL = " update $table " + " $colomn " + " $where "
        try {
            db.execSQL(SQL)
            Log.i(TAG, "SQL: " + SQL)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun delete(table: String, where: String) {
        var SQL = " delete from $table " + " $where "
        try {
            db.execSQL(SQL)
            Log.i(TAG, "SQL: " + SQL)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}