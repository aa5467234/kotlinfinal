package com.example.kotlinfinaldemo

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper


class MyDBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        val DATABASE_NAME = "final.db" // 資料庫名稱
        val DATABASE_VERSION = 1 // 資料庫版本
    }

    // 建立 account table 的語法
    var createAccountTable = "CREATE TABLE IF NOT EXISTS " + "account ( " + " userID TEXT, " + " password TEXT, " + " PRIMARY KEY (userID) " + ")"

    // 建立 sheet table 的語法
    var createSheetTable = "CREATE TABLE IF NOT EXISTS " + "sheet ( " + " frm_no TEXT, " + " sht_nam TEXT, " + " sht_tag TEXT, " + " insert_user TEXT, " + " PRIMARY KEY (frm_no) " + ")"

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(createAccountTable)
        db.execSQL(createSheetTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
    }
}