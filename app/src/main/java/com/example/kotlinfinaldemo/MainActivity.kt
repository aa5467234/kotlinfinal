package com.example.kotlinfinaldemo

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var context: Context = this
    lateinit var sqlite: SQLiteDB
    var TAG = "MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sqlite = SQLiteDB(context)

        imgbtnBack.setOnClickListener {
            finish()
        }

        // 註冊按鈕
        btnReg.setOnClickListener {
            var userID = etUserID.text.toString()
            var password = etPassword.text.toString()

            // 不能輸入空白
            if ("".equals(userID) || "".equals(password)) {
                Toast.makeText(context, "請輸入帳號或密碼！", Toast.LENGTH_SHORT).show()
            } else {
                var chkID = sqlite.query("account", "where userID = '" + userID + "'", "userID")

                // 檢查是否已註冊過
                if (userID.equals(chkID)) {
                    Toast.makeText(context, "帳號: " + userID + " 已經註冊過囉！！", Toast.LENGTH_SHORT).show()
                } else {
                    sqlite.insert("account", " userID, password", "'" + userID + "', " + "'" + password + "'")
                    Toast.makeText(context, "帳號: " + userID + ", 密碼: " + " 註冊成功！！", Toast.LENGTH_SHORT).show()
                }
            }
        }

        // 登入按紐
        btnLogin.setOnClickListener {
            var userID = etUserID.text.toString()
            var password = etPassword.text.toString()

            // 不能輸入空白
            if ("".equals(userID) || "".equals(password)) {
                Toast.makeText(context, "請輸入帳號或密碼！", Toast.LENGTH_SHORT).show()
            } else {
                // 檢查帳號密碼
                if ("".equals(sqlite.query("account", "where userID = '" + userID + "'", "userID"))) {
                    Toast.makeText(context, "帳號: " + userID + " 還沒註冊過哦！！", Toast.LENGTH_SHORT).show()
                } else {
                    if (!password.equals(sqlite.query("account", "where userID = '" + userID + "'", "password"))) {
                        Toast.makeText(context, " 密碼錯誤。", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(context, "帳號密碼正確！！", Toast.LENGTH_SHORT).show()

                        // 帳密正確跳轉至表單清單
                        val intent = Intent(this, SheetActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        }
    }
}

