package com.example.kotlinfinaldemo

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_sheet.*

class SheetActivity : AppCompatActivity() {
    val context = this
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sheet)

        btnRefresh.setOnClickListener {
            setListView()
            Toast.makeText(context, "刷新成功。", Toast.LENGTH_SHORT).show()
        }
        btnAddSht.setOnClickListener {
            // 跳轉至新增頁面
            val intent = Intent(this, AddSheetActivity::class.java)
            startActivity(intent)
        }

        setListView()
    }

    override fun onResume() {
        super.onResume()
        setListView()
    }

    private fun setListView() {
        var shtList = mutableListOf<Map<String, String>>()

        var sqLite = SQLiteDB(this)
        var cursor = sqLite.query("sheet", "")
        if (cursor != null) {
            if (cursor.count > 0) {
                cursor.moveToFirst()
                do {
                    var sht = mutableMapOf<String, String>()
                    sht.put("frm_no", cursor.getString(cursor.getColumnIndex("frm_no")))
                    sht.put("sht_nam", cursor.getString(cursor.getColumnIndex("sht_nam")))
                    sht.put("sht_tag", cursor.getString(cursor.getColumnIndex("sht_tag")))
                    sht.put("insert_user", cursor.getString(cursor.getColumnIndex("insert_user")))
                    shtList.add(sht)
                } while (cursor.moveToNext())
            }
            cursor.close()
        }
        Log.i("12663", shtList.toString())

        lvSht.adapter = ListViewAdapter(this, shtList)
        lvSht.setOnItemClickListener { adapterView, view, i, l ->
            val builder = AlertDialog.Builder(this)
            builder.setMessage("請選擇指令。")
            builder.setNeutralButton("修改") { dialog, which ->
                // 跳至修改頁面, 並額外傳入表單編號
                val intent = Intent(this, EditSheetActivity::class.java)
                intent.putExtra("frm_no", shtList.get(i).get("frm_no").toString())
                startActivity(intent)
            }
            builder.setNegativeButton("刪除") { dialog, which ->
                sqLite.delete("sheet", " where frm_no = '" + shtList.get(i).get("frm_no") + "'")
                setListView()
            }
            builder.setPositiveButton("取消", null)
            builder.show()
        }

    }
}